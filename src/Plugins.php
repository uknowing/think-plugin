<?php
declare(strict_types=1);

namespace think;

use think\App;
use think\helper\Str;
use think\facade\Config;
use think\facade\View;

abstract class Plugins
{
    // app 容器
    protected $app;
    // 请求对象
    protected $request;
    // 当前插件标识
    protected $name;
    // 插件路径
    protected $plugin_path;
    // 视图模型
    protected $view;
    // 插件配置
    protected $plugin_config;
    // 插件信息
    protected $plugin_info;

    /**
     * 插件构造函数
     * plugins constructor.
     * @param \think\App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->request = $app->request;
        $this->name = $this->getName();
        $this->plugin_path = $app->plugins->getpluginsPath() . $this->name . DIRECTORY_SEPARATOR;
        $this->plugin_config = "plugin_{$this->name}_config";
        $this->plugin_info = "plugin_{$this->name}_info";
        $this->view = $this->app->view;
        $this->view->engine()->layout(false);
        $this->view->config([
            'view_path' => $this->plugin_path . 'view' . DIRECTORY_SEPARATOR
        ]);
        if(file_exists($this->plugin_path.'vendor/autoload.php'))
        {
            require $this->plugin_path.'vendor/autoload.php';
        }
        // 控制器初始化
        $this->initialize();
    }

    // 初始化
    protected function initialize()
    {}

    /**
     * 获取插件标识
     * @return mixed|null
     */
    final protected function getName()
    {
        $class = get_class($this);
        list(, $name, ) = explode('\\', $class);
        $this->request->plugin = $name;

        return $name;
    }

    /**
     * 加载模板输出
     * @param string $template
     * @param array $vars           模板文件名
     * @return false|mixed|string   模板输出变量
     * @throws \think\Exception
     */
    protected function fetch($template = '', $vars = [])
    {
        return $this->view->fetch($template, $vars);
    }

    /**
     * 渲染内容输出
     * @access protected
     * @param  string $content 模板内容
     * @param  array  $vars    模板输出变量
     * @return mixed
     */
    protected function display($content = '', $vars = [])
    {
        return $this->view->display($content, $vars);
    }

    /**
     * 模板变量赋值
     * @access protected
     * @param  mixed $name  要显示的模板变量
     * @param  mixed $value 变量的值
     * @return $this
     */
    protected function assign($name, $value = '')
    {
        $this->view->assign([$name => $value]);

        return $this;
    }

    /**
     * 初始化模板引擎
     * @access protected
     * @param  array|string $engine 引擎参数
     * @return $this
     */
    protected function engine($engine)
    {
        $this->view->engine($engine);

        return $this;
    }

    /**
     * 插件基础信息
     * @return array
     */
    final public function getInfo()
    {
        $info = Config::get($this->plugin_info, []);
        if ($info) {
            return $info;
        }

        // 文件属性
        $info = $this->info ?? [];
        // 文件配置
        $info_file = $this->plugin_path . 'info.ini';
        if (is_file($info_file)) {
            $_info = parse_ini_file($info_file, true, INI_SCANNER_TYPED) ?: [];
            $_info['url'] = plugins_url();
            $info = array_merge($info, $_info);
        }
        Config::set($info, $this->plugin_info);

        return isset($info) ? $info : [];
    }

    /**
     * 插件基础菜单
     * @return array
     */
    final public function getMenu()
    {
        return $this->menu ?? [];
    }

    public function checkConfigGroup($config): bool
    {
        // 获取第一个元素
        $arrayShift = array_shift($config);
        if (array_key_exists('title', $arrayShift) && array_key_exists('type', $arrayShift)) {
            // 未开启分组
            return false;
        } else {
            // 开启分组
            return true;
        }
    }

    /**
     * 获取配置信息
     * @param bool $type 是否获取完整配置
     * @return array|mixed
     */
    final public function getConfig($type = false)
    {
        $config = Config::get($this->plugin_config, []);
        if ($config) {
            return $config;
        }
        $config_file = $this->plugin_path . 'config.php';
        if (is_file($config_file)) {
            $temp_arr = (array)include $config_file;
            if ($type) {
                return $temp_arr;
            }

            if($this->checkConfigGroup($temp_arr))
            {
                foreach ($temp_arr as $key=>$val)
                {
                    foreach ($val as $k=>$v)
                    {
                        if (in_array($v['type'], ['select','checkbox'])) {
                            $v['value'] = explode(',', $v['value']);
                        }
                        if ($v['type'] == 'array') {
                            $v['value'] = json_decode($v['option'],true);
                        }
                        $config[$k]=$v['value'];
                    }
                }
            }else{
                foreach ($temp_arr as $key=>$val)
                {
                    if (in_array($val['type'], ['select','checkbox'])) {
                        $val['value'] = explode(',', $val['value']);
                    }

                    if ($val['type'] == 'array') {
                        $v['value'] = json_decode($val['option'],true);
                    }

                    $config[$key]=$val['value'];
                }
            }
        }

        Config::set($config, $this->plugin_config);

        return $config;
    }

    //必须实现安装
    abstract public function install();

    //必须卸载插件方法
    abstract public function uninstall();
}
